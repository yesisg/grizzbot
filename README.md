
## Found Grizz

Site created in case our dog Grizz was ever lost and to provide up to date contact information - https://grizzbot.club/.

Copied for other dog Murphy - [source](https://gitlab.com/yesi.sg/murphalerf)


## Source
This project was forked from https://gitlab.com/pages/plain-html/ and css
is sourced from https://github.com/yasoob/yasoob.github.io

1. CSS is missing! That means that you have wrongly set up the CSS URL in your
   HTML files. Have a look at the [index.html] for an example.

[ci]: https://about.gitlab.com/gitlab-ci/
[index.html]: https://gitlab.com/pages/plain-html/blob/master/public/index.html
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
